# What is OmiseGO (OMG)?

OmiseGO is currently an Ethereum token, which can be bought and sold through multiple exchanges.
The OMG token currently runs on the Ethereum blockchain, but will be transferred to a separate
blockchain in the future.

Existing OmiseGO tokens have either been bought during a crowdsale, or obtained through a coindrop.

OmiseGO and its app ecosystem has yet to be fully released, but will be released by 2018. Apps will allow
anybody with a smartphone or access to a computer the ability to take full advantage of the OMG cryptocurrency
network.

# Who is developing OmiseGO?
Omise is the company behind OmiseGO (OMG) token and the future Omise cryptocurrency platform.
Omise is a company based in Southeast Asian payment system company, based in Thailand.
The company currently develops a SDK others can use to enable simple payments with the 
assurance that payees' information is secure. The Omise SDK offers features like one-click
payments, and payment requests. Omise's services are analagous to those of a company like Paypal - 
the only difference is that Omise is widely used in developing Asian countries.

Omise has partnered with Alipay, China's largest internet-based payment system, similar to Venmo,
in order to increase Omise's coverage in China. 

Omise currently processes large transfers between individuals and businesses every day
    -9,000 businesses
    -“At the time of writing, we process eight to nine digits (USD) worth of transactions per day.”
    -10,000,000 - 100,000,000 USD or more

# Why use OmiseGO?

OmiseGO aims to solve problems other cryptocurrencies have faced in the area of fast, instantaneous
and simple to use payments, whether they are used in businesses, casual payments, or to even pay out
salaries.

Automated contracts will allow employers of any sort to pay their employees automatically, with little
hassle due to OMG's lack of use of central banking systems.

 Additionally, because OmiseGO cryptocurrencies do not require bank accounts like credit and debit cards,
 it is easier for citizens of developing countries to join OMG's network. Omise, the company developing
 the OMG token, already has significant penetration in developing Southeast Asian countries
 (see Who is developing OmiseGO?), and hopes to increase financial participation in these countries
 with the launch of the OMG token. 

 Omise also aims to make their OMG token system an easy way to transfer and exchange fiat currencies.
 It should be easy for anybody to purchase currencies, crypto or fiat. Conversely, it should be easy
 for anybody to sell their accumulated currencies quickly and without large fees.

 Because Omise is already in use in many Southeast Asian countries (again, see Who is developing OmiseGO?), 
 the company plans to create an Omise wallet SDK. This will make it easy for any company to incorporate 
 the OmiseGO platform into their products, website, or apps. 