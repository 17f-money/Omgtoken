---
title: Why use OmiseGO?
subtitle: Unbank the Banked with Ethereum
date: 2017-11-02
draft: true
---
OmiseGO aims to solve problems other cryptocurrencies have faced in the area of fast, instantaneous
and simple to use payments, whether they are used in businesses, casual payments, or to even pay out
salaries.

Automated contracts will allow employers of any sort to pay their employees automatically, with little
hassle due to OMG's lack of use of central banking systems.

 Additionally, because OmiseGO cryptocurrencies do not require bank accounts like credit and debit cards,
 it is easier for citizens of developing countries to join OMG's network. Omise, the company developing
 the OMG token, already has significant penetration in developing Southeast Asian countries
 (see Who is developing OmiseGO?), and hopes to increase financial participation in these countries
 with the launch of the OMG token. 

 Omise also aims to make their OMG token system an easy way to transfer and exchange fiat currencies.
 It should be easy for anybody to purchase currencies, crypto or fiat. Conversely, it should be easy
 for anybody to sell their accumulated currencies quickly and without large fees.

 Because Omise is already in use in many Southeast Asian countries (again, see Who is developing OmiseGO?), 
 the company plans to create an Omise wallet SDK. This will make it easy for any company to incorporate 
 the OmiseGO platform into their products, website, or apps. 

