---
title: OmiseGO (OMG) Stats?
subtitle: Unbank the Banked with Ethereum
date: 2017-11-02
---
As of early November 2017
OmiseGO
    Market cap - 983,189,525.40 USD
    Total supply - 140,245,398.2451 OMG
    Coin price - 7.01 USD
    Token holders - 525047 

OmiseGO is currently fairly active and multiple trades happen each minute, even before its full release.


