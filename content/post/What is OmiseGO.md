---
title: What is OmiseGO (OMG)?
subtitle: Unbank the Banked with Ethereum
date: 2017-11-02
draft: true
---
What is OmiseGO (OMG)?

OmiseGO is currently an Ethereum token, which can be bought and sold through multiple exchanges.
The OMG token currently runs on the Ethereum blockchain, but will be transferred to a separate
blockchain in the future.

Existing OmiseGO tokens have either been bought during a crowdsale, or obtained through a coindrop.

OmiseGO and its app ecosystem has yet to be fully released, but will be released by 2018. Apps will allow
anybody with a smartphone or access to a computer the ability to take full advantage of both the OMG cryptocurrency
network and Omise's existing payment systems.
