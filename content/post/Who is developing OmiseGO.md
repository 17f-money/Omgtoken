---
title: Who is developing OmiseGO?
subtitle: Southeast Asian Giant
date: 2017-11-02
draft: true
---
Omise is the company behind OmiseGO (OMG) token and the future Omise cryptocurrency platform.
Omise is a company based in Southeast Asian payment system company, based in Thailand.
The company currently develops a SDK others can use to enable simple payments with the 
assurance that payees' information is secure. The Omise SDK offers features like one-click
payments, and payment requests. Omise's services are analagous to those of a company like Paypal - 
the only difference is that Omise is widely used in developing Asian countries.

Omise has partnered with Alipay, China's largest internet-based payment system, similar to Venmo,
in order to increase Omise's coverage in China. 

Omise currently processes large transfers between individuals and businesses every day
    - 9,000 businesses
    - “At the time of writing, we process eight to nine digits (USD) worth of transactions per day.”
    - 10,000,000 - 100,000,000 USD or more


